# Atelier Robobrosse

Un atelier simple et ludique, où la tête d'une vieille brosse à dents devient un robot capable de sortir d'un labyrinthe.
Le projet permet de faire de la médiation autour de l'impression 3D (la pièce ne met que 10 minutes à s'imprimer), et faire rentrer dans la fabrication et la bidouille les enfants à partir de 6/7 ans.
Le labyrinthe coupé au laser est réorganisable. Le moteur vibrant est de ce type là: https://www.gotronic.fr/art-vibreur-miniature-vm0612-21053.htm
![](pics/2019-06-22-IMG_7960.jpg)
![](pics/2019-06-22-IMG_7963.jpg)

Atelier proposé pour la première fois en juin 2019 dans le cadre du festival Bien Urbain, à Besançon.